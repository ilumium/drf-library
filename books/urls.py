from django.urls import path

from books import views

urlpatterns = [
    path("", views.BookAPIView.as_view(), name="book_list"),
]
