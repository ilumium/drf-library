from rest_framework.views import APIView
from rest_framework.response import Response
from books.models import Book
from books.serializers import BookSerializer


# Create your views here.
class BookAPIView(APIView):
    # queryset = Book.objects.all()
    serializer_class = BookSerializer

    def get(self, request):
        books = Book.objects.all()
        serializer = BookSerializer(books, many=True)
        return Response(serializer.data)

    def post(self, request):
        book = request.data
        serializer = BookSerializer(data=book)
        if serializer.is_valid(raise_exception=True):
            book_saved = serializer.save()
        return Response(
            {"success": "Book '{}' created successfully".format(book_saved.title)}
        )
